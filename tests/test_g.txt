TEST: Tests the 'g' (give state) command

> g
> $dup-line 18 . . . . . . . . . .
> t t t . s l l . z j
> . t o o s s l z z j
> . . o o . s l z j j
> i i i i . . i i i i

> p

# output

$dup-line 18 . . . . . . . . . .
t t t . s l l . z j
. t o o s s l z z j
. . o o . s l z j j
i i i i . . i i i i